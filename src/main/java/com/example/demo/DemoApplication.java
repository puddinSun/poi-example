package com.example.demo;

import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.configurationprocessor.json.JSONArray;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SpringBootApplication
@Slf4j
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
        JSONArray files = processFile("/Users/sunn/KPI.2021.NTThuy8.xlsx");
        Set<String> filesInFolder = processFolder("/Users/sunn/offer_letter");

        try {
            List<String> missingCandidates = findMissingCandidates(files, filesInFolder);
            log.info("Missing candidates ==> {}", missingCandidates);
        } catch (JSONException ex) {
            log.error("Error when parsing " + ex.getMessage());
        }

    }

    private static List<String> findMissingCandidates(JSONArray rows, Set<String> filesInFolder) throws JSONException {
        List<String> names = new ArrayList<>();

        Set<String> nameVietnamese = filesInFolder.stream().map(e -> {
            String[] s = e.split("_");
            String s1 = VietnamCharUtil.removeAccent(s[2]);
            String trim = s1.substring(4).trim();
            if (trim.contains(".pdf")) {
                trim = trim.substring(0, trim.length() - 4);
            }
            return trim.toLowerCase();
        }).collect(Collectors.toSet());

        List<String> rowStrings = new ArrayList<>();
        for (int i = 0; i < rows.length(); i++) {
            JSONObject jsonObject = rows.getJSONObject(i);
            String fullName = VietnamCharUtil.removeAccent(jsonObject.getString("Full name"));
            jsonObject.put("Full name", fullName.trim().toLowerCase());
            rows.put(i, jsonObject);
            rowStrings.add(fullName.trim().toLowerCase());
        }

        for (String eachName : nameVietnamese) {
            if (!rowStrings.contains(eachName)) {
                names.add(eachName);
            }
        }

        return names;
    }

    private static Set<String> processFolder(String folderPath) {
        return Stream.of(Objects.requireNonNull(new File(folderPath).listFiles()))
                .filter(file -> !file.isDirectory())
                .map(File::getName)
                .collect(Collectors.toSet());
    }

    private static JSONArray processFile(String filePath) {
        log.info("Begin process ...");
        try (
                FileInputStream file = new FileInputStream(filePath);
                XSSFWorkbook workbook = new XSSFWorkbook(file)
        ) {

            //Get first/desired sheet from the workbook
            XSSFSheet sheet = workbook.getSheetAt(0);

            //Iterate through each rows one by one
            List<String> titles = new ArrayList<>();

            XSSFRow titleRow = sheet.getRow(2);
            Iterator<Cell> titleIterator = titleRow.cellIterator();
            while (titleIterator.hasNext()) {
                titles.add(titleIterator.next().getStringCellValue());
            }
            int index = -1;
            int controlIndex = 0;

            JSONArray array = new JSONArray();

            for (Row row : sheet) {
                index++;
                if (index <= 3) {
                    continue;
                }
                //For each row, iterate through all the columns
                JSONObject jsonObject = new JSONObject();
                Iterator<Cell> cellIterator = row.cellIterator();

                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    //Check the cell type and format accordingly
                    CellType cellType = cell.getCellType();
                    switch (cellType) {
                        case NUMERIC:
                            jsonObject.put(titles.get(controlIndex), cell.getNumericCellValue() + "");
                            break;
                        case STRING:
                            jsonObject.put(titles.get(controlIndex), cell.getStringCellValue());
                            break;
                        default:
                            break;
                    }
                    controlIndex++;
                }
                array.put(jsonObject);
                controlIndex = 0;
            }
            log.info("End process!");
            return array;
        } catch (Exception e) {
            e.printStackTrace();
            return new JSONArray();
        }
    }

}
